// SobelAVX2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <opencv2/opencv.hpp>
#include <benchmark/benchmark.h>
#include <immintrin.h>
#pragma comment ( lib, "Shlwapi.lib" )

static void SobelGradientOpenCV(
    uint8_t* imagePtr,
    int16_t* gradientXPtr,
    int16_t* gradientYPtr,
    int width,
    int height)
{
     const auto inputImage = cv::Mat{ height, width, CV_8UC1, static_cast<void*>(imagePtr) };
     const auto outputImage = cv::Mat{ height, width, CV_16SC1, !!gradientXPtr ? static_cast<void*>(gradientXPtr) : static_cast<void*>(gradientYPtr) };

     cv::Sobel(inputImage, outputImage, CV_16SC1, !!gradientXPtr, !!gradientYPtr);
}

// describes gradients which need to be computed.
enum class Grad
{
    X = 1, // calculate only X gradient
    Y = 2, // calculate only Y gradient
    XY = X | Y // calculate both X and Y gradients
};

template<Grad gradType>
static void SobelGradientSIMD(
    uint8_t* imagePtr,
    int16_t* gradientXPtr,
    int16_t* gradientYPtr,
    int32_t width,
    int32_t height
)
{
    constexpr auto ElementCount = int32_t{ 32 };
    constexpr auto calcXGrad = (static_cast<int>(gradType) & static_cast<int>(Grad::X)) > 0;
    constexpr auto calcYGrad = (static_cast<int>(gradType) & static_cast<int>(Grad::Y)) > 0;
    for (auto y = int32_t{ 1 }; y < height - 1; y++)
    {
        auto x = int32_t{ 1 };
        //Here we check for the next free 32 elements beforehand
        //Shifting x by 28 values (without the last 4 due to shift from 2 lanes)
        for (; x + ElementCount < width - 1; x += ElementCount - 4)
        {
            const auto currentPixel = width * y + x - 1;
            const auto prevRowPixel = currentPixel - width;
            const auto nextRowPixel = currentPixel + width;

            //Moving gradients data to _m256i
            //We'll read all 3 rows of data from the image for the first row
            const auto imagePrev = _mm256_lddqu_si256(reinterpret_cast<const __m256i*>(imagePtr + prevRowPixel));
            const auto imageMidd = _mm256_lddqu_si256(reinterpret_cast<const __m256i*>(imagePtr + currentPixel));
            const auto imageNext = _mm256_lddqu_si256(reinterpret_cast<const __m256i*>(imagePtr + nextRowPixel));
            //Converting to 16 bit to avoid overflow
            const auto image16Prev0 = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(imagePrev, 0));
            const auto image16Midd0 = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(imageMidd, 0));
            //For the second parts we'll need to shift the previous ones by 4 bytes to remove the two 0 values in the end result as we are only calculating the 14 out of 16 values
            //For this we are doing full 256bit register shift not just separate lanes
            //For this we'll need to do this not just a _mm256_slli_si256, as the two 128 bit lanes are shifted separately
            //Thus we'll have to do the shuffling so that the _mm256_alignr_epi8 method would shift the leftover data
            //after which we are left with correct linked data in both of the lanes
            //The formula is:
            // 0 < N < 16   _mm256_alignr_epi8(A, _mm256_permute2x128_si256(A, A, _MM_SHUFFLE(0, 0, 2, 0)), 16 - N)
            // N = 16       _mm256_permute2x128_si256(A, A, _MM_SHUFFLE(0, 0, 2, 0))
            // 16 < N < 32  _mm256_slli_si256(_mm256_permute2x128_si256(A, A, _MM_SHUFFLE(0, 0, 2, 0)), N - 16)
            const auto image16PrevShifted = _mm256_alignr_epi8(imagePrev, _mm256_permute2x128_si256(imagePrev, imagePrev, _MM_SHUFFLE(0, 0, 2, 0)), 14);
            const auto image16Prev1 = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(image16PrevShifted, 1));
            const auto image16MiddShifted = _mm256_alignr_epi8(imageMidd, _mm256_permute2x128_si256(imageMidd, imageMidd, _MM_SHUFFLE(0, 0, 2, 0)), 14);
            const auto image16Midd1 = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(image16MiddShifted, 1));
            //For the second parts we'll need to shift the previous ones by 4 bytes to remove the two 0 values in the end result as we are only calculating the 14 out of 16 values
            const auto image16Next0 = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(imageNext, 0));
            const auto image16NextShifted = _mm256_alignr_epi8(imageNext, _mm256_permute2x128_si256(imageNext, imageNext, _MM_SHUFFLE(0, 0, 2, 0)), 14);
            const auto image16Next1 = _mm256_cvtepu8_epi16(_mm256_extracti128_si256(image16NextShifted, 1));

            //Doing Y stuff
            if constexpr (calcYGrad)
            {
                //Basically the logic is to have one row of an image stored in 3 separate registers with shifted data to do operations for example
                //     1 2 3
                //   1 2 3 4 //This will be multiplied by 2
                // 1 2 3 4 5

                //Then while we are on the imageMidd that is the current image pixel, we can calculate gradient values for both previous and next image data rows
                //For that we first will calculate the sum for both of the previous and next image data rows
                //And then will do the subtraction of the previous row from next
                //Thus we will calculate the Sobel gradient Y for the current image pixel

                //Doing full 256 bit shift not just separate lanes
                //For this we'll need to do this not just a _mm256_slli_si256, as the two 128 bit lanes are shifted separately
                //Thus we'll have to do the shuffling so that the _mm256_alignr_epi8 method would shift the leftover data
                //after which we are left with correct linked data in both of the lanes
                //The formula is:
                // 0 < N < 16   _mm256_alignr_epi8(A, _mm256_permute2x128_si256(A, A, _MM_SHUFFLE(0, 0, 2, 0)), 16 - N)
                // N = 16       _mm256_permute2x128_si256(A, A, _MM_SHUFFLE(0, 0, 2, 0))
                // 16 < N < 32  _mm256_slli_si256(_mm256_permute2x128_si256(A, A, _MM_SHUFFLE(0, 0, 2, 0)), N - 16)

                //Converting and shifting data for previous data row
                const auto yGrad16Prev01 = _mm256_alignr_epi8(image16Prev0, _mm256_permute2x128_si256(image16Prev0, image16Prev0, _MM_SHUFFLE(0, 0, 2, 0)), 14);
                const auto yGrad16Prev02 = _mm256_alignr_epi8(image16Prev0, _mm256_permute2x128_si256(image16Prev0, image16Prev0, _MM_SHUFFLE(0, 0, 2, 0)), 12);
                const auto yGrad16Prev11 = _mm256_alignr_epi8(image16Prev1, _mm256_permute2x128_si256(image16Prev1, image16Prev1, _MM_SHUFFLE(0, 0, 2, 0)), 14);
                const auto yGrad16Prev12 = _mm256_alignr_epi8(image16Prev1, _mm256_permute2x128_si256(image16Prev1, image16Prev1, _MM_SHUFFLE(0, 0, 2, 0)), 12);
                //Converting and shifting data for next data row
                const auto yGrad16Next01 = _mm256_alignr_epi8(image16Next0, _mm256_permute2x128_si256(image16Next0, image16Next0, _MM_SHUFFLE(0, 0, 2, 0)), 14);
                const auto yGrad16Next02 = _mm256_alignr_epi8(image16Next0, _mm256_permute2x128_si256(image16Next0, image16Next0, _MM_SHUFFLE(0, 0, 2, 0)), 12);
                const auto yGrad16Next11 = _mm256_alignr_epi8(image16Next1, _mm256_permute2x128_si256(image16Next1, image16Next1, _MM_SHUFFLE(0, 0, 2, 0)), 14);
                const auto yGrad16Next12 = _mm256_alignr_epi8(image16Next1, _mm256_permute2x128_si256(image16Next1, image16Next1, _MM_SHUFFLE(0, 0, 2, 0)), 12);
                //Multiplying middle rows (shifted by one value right) by 2 for both previous and next image data registers
                const auto yGrad16Prev01Mul = _mm256_slli_epi16(yGrad16Prev01, 1);
                const auto yGrad16Prev11Mul = _mm256_slli_epi16(yGrad16Prev11, 1);
                const auto yGrad16Next01Mul = _mm256_slli_epi16(yGrad16Next01, 1);
                const auto yGrad16Next11Mul = _mm256_slli_epi16(yGrad16Next11, 1);
                //Now we can add them to obtain the sum that we will subrtact later on
                const auto yResAddPrev0 = _mm256_add_epi16(_mm256_add_epi16(image16Prev0, yGrad16Prev01Mul), yGrad16Prev02);
                const auto yResAddPrev1 = _mm256_add_epi16(_mm256_add_epi16(image16Prev1, yGrad16Prev11Mul), yGrad16Prev12);
                const auto yResAddNext0 = _mm256_add_epi16(_mm256_add_epi16(image16Next0, yGrad16Next01Mul), yGrad16Next02);
                const auto yResAddNext1 = _mm256_add_epi16(_mm256_add_epi16(image16Next1, yGrad16Next11Mul), yGrad16Next12);
                //Now we'll need to subtract the next from the prev
                const auto yResSub0 = _mm256_sub_epi16(yResAddNext0, yResAddPrev0);
                const auto yResSub1 = _mm256_sub_epi16(yResAddNext1, yResAddPrev1);

                //Doing right shift to store resulting 28 values
                //Basically, we'll need to shift all of the 32 bytes not two separate lanes with _mm256_srli_si256
                //For this we again have to do the shuffling so that the _mm256_alignr_epi8 method would shift the leftover data
                //But this time vice versa as we are doing the right shift
                //The formula is:
                // 0 < N < 16   _mm256_alignr_epi8(_mm256_permute2x128_si256(A, A, _MM_SHUFFLE(2, 0, 0, 1)), A, N)
                // N = 16       _mm256_permute2x128_si256(A, A, _MM_SHUFFLE(2, 0, 0, 1))
                // 16 < N < 32  _mm256_srli_si256(_mm256_permute2x128_si256(A, A, _MM_SHUFFLE(2, 0, 0, 1)), N - 16)
                const auto yRes0 = _mm256_alignr_epi8(_mm256_permute2x128_si256(yResSub0, yResSub0, _MM_SHUFFLE(2, 0, 0, 1)), yResSub0, 4);
                const auto yRes1 = _mm256_alignr_epi8(_mm256_permute2x128_si256(yResSub1, yResSub1, _MM_SHUFFLE(2, 0, 0, 1)), yResSub1, 4);
                //Writing Y gradients
                auto* currGradientYPixel = gradientYPtr + currentPixel + 1;
                _mm256_storeu_si256(reinterpret_cast<__m256i*>(currGradientYPixel), yRes0);
                _mm256_storeu_si256(reinterpret_cast<__m256i*>(currGradientYPixel + 14), yRes1);
            }
            //Now we can calculate all of the X stuff
            if constexpr (calcXGrad)
            {
                //Doing the X gradients is easier, because the data is already in the registers - no need to shift for each of them
                //For calculations we'll need to first multiply the values from the middle row (one with current pixel)
                //Then we'll add the results to previous and next registers so that all that would be left is to subtract them by using the shifting technique again

                //Multiplying the middle lane by 2
                const auto xGrad16Midd0Mul = _mm256_slli_epi16(image16Midd0, 1);
                const auto xGrad16Midd1Mul = _mm256_slli_epi16(image16Midd1, 1);
                //Adding all of the lines
                const auto xResAddAll0 = _mm256_add_epi16(_mm256_add_epi16(image16Prev0, image16Next0), xGrad16Midd0Mul);
                const auto xResAddAll1 = _mm256_add_epi16(_mm256_add_epi16(image16Prev1, image16Next1), xGrad16Midd1Mul);
                //Now what we need to do is shift one row with fully added values by two values right and subtract it from the original added values
                //This will give us a desired values that are the xGradient
                //The result will be shifted by 2
                //     1 2 3
                // 1 2 3 4 5
                // res[1] = 3 - 1;
                //Again doing full 32bit register shift as described before
                const auto xResAddShifted0 = _mm256_alignr_epi8(xResAddAll0, _mm256_permute2x128_si256(xResAddAll0, xResAddAll0, _MM_SHUFFLE(0, 0, 2, 0)), 12);
                const auto xResAddShifted1 = _mm256_alignr_epi8(xResAddAll1, _mm256_permute2x128_si256(xResAddAll1, xResAddAll1, _MM_SHUFFLE(0, 0, 2, 0)), 12);
                //Subtracting the lines
                const auto xResSub0 = _mm256_sub_epi16(xResAddAll0, xResAddShifted0);
                const auto xResSub1 = _mm256_sub_epi16(xResAddAll1, xResAddShifted1);
                //Doing right shift to store resulting 28 values as described before
                const auto xRes0 = _mm256_alignr_epi8(_mm256_permute2x128_si256(xResSub0, xResSub0, _MM_SHUFFLE(2, 0, 0, 1)), xResSub0, 4);
                const auto xRes1 = _mm256_alignr_epi8(_mm256_permute2x128_si256(xResSub1, xResSub1, _MM_SHUFFLE(2, 0, 0, 1)), xResSub1, 4);
                auto* currGradientXPixel = gradientXPtr + currentPixel + 1;
                _mm256_storeu_si256(reinterpret_cast<__m256i*>(currGradientXPixel), xRes0);
                _mm256_storeu_si256(reinterpret_cast<__m256i*>(currGradientXPixel + 14), xRes1);
            }
        }
        //Doing further calculations for the rest of the data
        for (; x < width - 1; x++)
        {
            const auto curPixel = width * y + x;
            const auto pixelPtr = imagePtr + curPixel;
            const auto nextRowPixelPtr = pixelPtr + width;
            const auto prevRowPixelPtr = pixelPtr - width;

            if constexpr (calcXGrad)
            {
                gradientXPtr[curPixel] = static_cast<int16_t>(((pixelPtr[1] - pixelPtr[-1]) << 1) +
                    nextRowPixelPtr[1] - nextRowPixelPtr[-1] +
                    prevRowPixelPtr[1] - prevRowPixelPtr[-1]);
            }
            if constexpr (calcYGrad)
            {
                gradientYPtr[curPixel] = static_cast<int16_t>(((nextRowPixelPtr[0] - prevRowPixelPtr[0]) << 1) +
                    nextRowPixelPtr[1] - prevRowPixelPtr[1] +
                    nextRowPixelPtr[-1] - prevRowPixelPtr[-1]);
            }
        }
    }
}

template<Grad gradType>
static void SobelGradientCustom(
    uint8_t* imagePtr,
    int16_t* gradientXPtr,
    int16_t* gradientYPtr,
    int32_t width,
    int32_t height)
{
    constexpr auto calcXGrad = (static_cast<int>(gradType) & static_cast<int>(Grad::X)) > 0;
    constexpr auto calcYGrad = (static_cast<int>(gradType) & static_cast<int>(Grad::Y)) > 0;
    for (auto row = 1; row < height - 1; ++row)
    {
        for (auto col = 1; col < width - 1; ++col)
        {
            const auto curPixel = width * row + col;
            const auto pixelPtr = imagePtr + curPixel;
            const auto nextRowPixelPtr = pixelPtr + width;
            const auto prevRowPixelPtr = pixelPtr - width;
            if constexpr (calcXGrad)
            {
                gradientXPtr[curPixel] = static_cast<int16_t>(((pixelPtr[1] - pixelPtr[-1]) << 1) +
                    nextRowPixelPtr[1] - nextRowPixelPtr[-1] +
                    prevRowPixelPtr[1] - prevRowPixelPtr[-1]);
            }
            if constexpr (calcYGrad)
            {
                gradientYPtr[curPixel] = static_cast<int16_t>(((nextRowPixelPtr[0] - prevRowPixelPtr[0]) << 1) +
                    nextRowPixelPtr[1] - prevRowPixelPtr[1] +
                    nextRowPixelPtr[-1] - prevRowPixelPtr[-1]);
            }
        }
    }
}

static void FillImage(std::vector<uint8_t>& image)
{
    srand(42);
    for (auto& it : image)
    {
        it = static_cast<uint8_t>(rand() % 256);
    }
}

static bool CompareImages(const int16_t* gradA, const int16_t* gradB, int32_t width, int32_t height)
{
    for (int row = 1; row < height - 1; ++row)
    {
        for (int col = 1; col < width - 1; ++col)
        {
            const auto pos = row * width + col;
            if (gradA[pos] != gradB[pos])
            {
                return false;
            }
        }
    }
    return true;
}

constexpr auto width = int32_t{ 1000 };
constexpr auto height = int32_t{ 800 };
constexpr auto fullSize = width * height;

int main(int argc, char** argv)
{
    std::vector<uint8_t> image(fullSize);
    FillImage(image);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    std::vector<int16_t> gradCustomX(fullSize);
    std::vector<int16_t> gradCustomY(fullSize);
    std::vector<int16_t> gradSIMDX(fullSize);
    std::vector<int16_t> gradSIMDY(fullSize);

    SobelGradientOpenCV(image.data(), gradX.data(), 0, width, height);
    SobelGradientOpenCV(image.data(), 0, gradY.data(), width, height);
    SobelGradientCustom<Grad::XY>(image.data(), gradCustomX.data(), gradCustomY.data(), width, height);
    SobelGradientSIMD<Grad::XY>(image.data(), gradSIMDX.data(), gradSIMDY.data(), width, height);

    const auto isXEqual = CompareImages(gradX.data(), gradCustomX.data(), width, height)
        && CompareImages(gradX.data(), gradSIMDX.data(), width, height);
    const auto isYEqual = CompareImages(gradY.data(), gradCustomY.data(), width, height)
        && CompareImages(gradY.data(), gradSIMDY.data(), width, height);

    if (isXEqual && isYEqual)
    {
        std::cout << "Data Correct!" << std::endl;
    }
    else
    {
        std::cout << "Data is not equal!" << std::endl;
        return 1;
    }

    ::benchmark::Initialize(&argc, argv);

    if (::benchmark::ReportUnrecognizedArguments(argc, argv))
    {
        return 1;
    }

    ::benchmark::RunSpecifiedBenchmarks();
}

static void XGradientOpenCV(benchmark::State& state)
{
    std::vector<uint8_t> image(fullSize);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    FillImage(image);

    for ([[maybe_unused]] auto _state : state)
    {
        SobelGradientOpenCV(image.data(), gradX.data(), 0, width, height);
    }
}
BENCHMARK(XGradientOpenCV)->Unit(benchmark::TimeUnit::kMicrosecond)->MinTime(5);


static void XGradientRaw(benchmark::State& state)
{
    std::vector<uint8_t> image(fullSize);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    FillImage(image);

    for ([[maybe_unused]] auto _state : state)
    {
        SobelGradientCustom<Grad::X>(image.data(), gradX.data(), gradY.data(), width, height);
    }
}
BENCHMARK(XGradientRaw)->Unit(benchmark::TimeUnit::kMicrosecond)->MinTime(5);

static void XGradientSIMD(benchmark::State& state)
{
    std::vector<uint8_t> image(fullSize);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    FillImage(image);

    for ([[maybe_unused]] auto _state : state)
    {
        SobelGradientSIMD<Grad::X>(image.data(), gradX.data(), gradY.data(), width, height);
    }
}
BENCHMARK(XGradientSIMD)->Unit(benchmark::TimeUnit::kMicrosecond)->MinTime(5);

static void YGradientOpenCV(benchmark::State& state)
{
    std::vector<uint8_t> image(fullSize);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    FillImage(image);

    for ([[maybe_unused]] auto _state : state)
    {
        SobelGradientOpenCV(image.data(), 0, gradY.data(), width, height);
    }
}
BENCHMARK(YGradientOpenCV)->Unit(benchmark::TimeUnit::kMicrosecond)->MinTime(5);


static void YGradientRaw(benchmark::State& state)
{
    std::vector<uint8_t> image(fullSize);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    FillImage(image);

    for ([[maybe_unused]] auto _state : state)
    {
        SobelGradientCustom<Grad::Y>(image.data(), gradX.data(), gradY.data(), width, height);
    }
}
BENCHMARK(YGradientRaw)->Unit(benchmark::TimeUnit::kMicrosecond)->MinTime(5);

static void YGradientSIMD(benchmark::State& state)
{
    std::vector<uint8_t> image(fullSize);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    FillImage(image);

    for ([[maybe_unused]] auto _state : state)
    {
        SobelGradientSIMD<Grad::Y>(image.data(), gradX.data(), gradY.data(), width, height);
    }
}
BENCHMARK(YGradientSIMD)->Unit(benchmark::TimeUnit::kMicrosecond)->MinTime(5);

static void XYGradientOpenCV(benchmark::State& state)
{
    std::vector<uint8_t> image(fullSize);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    FillImage(image);

    for ([[maybe_unused]] auto _state : state)
    {
        SobelGradientOpenCV(image.data(), gradX.data(), 0, width, height);
        SobelGradientOpenCV(image.data(), 0, gradY.data(), width, height);
    }
}
BENCHMARK(XYGradientOpenCV)->Unit(benchmark::TimeUnit::kMicrosecond)->MinTime(5);


static void XYGradientRaw(benchmark::State& state)
{
    std::vector<uint8_t> image(fullSize);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    FillImage(image);

    for ([[maybe_unused]] auto _state : state)
    {
        SobelGradientCustom<Grad::XY>(image.data(), gradX.data(), gradY.data(), width, height);
    }
}
BENCHMARK(XYGradientRaw)->Unit(benchmark::TimeUnit::kMicrosecond)->MinTime(5);

static void XYGradientSIMD(benchmark::State& state)
{
    std::vector<uint8_t> image(fullSize);
    std::vector<int16_t> gradX(fullSize);
    std::vector<int16_t> gradY(fullSize);
    FillImage(image);

    for ([[maybe_unused]] auto _state : state)
    {
        SobelGradientSIMD<Grad::XY>(image.data(), gradX.data(), gradY.data(), width, height);
    }
}
BENCHMARK(XYGradientSIMD)->Unit(benchmark::TimeUnit::kMicrosecond)->MinTime(5);

